//
//  ViewController.h
//  DatePicker
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/23.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
//建立UIDatePicker關聯，型態是Outlet
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

//建立Label關聯，型態是Outlet。此為顯示挑選的日期
@property (weak, nonatomic) IBOutlet UILabel *showDate;

//建立Label關聯，型態是Outlet。此為顯示年齡
@property (weak, nonatomic) IBOutlet UILabel *age;

//建立Button關聯，型態是Action。此為DatePicker的開關
- (IBAction)selectDateBtn:(id)sender;

//建立UIDatePicker關聯，型態為Action
- (IBAction)datePickerChange:(id)sender;
@end

