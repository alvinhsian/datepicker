//
//  ViewController.m
//  DatePicker
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/23.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectDateBtn:(id)sender {
    
    //當有人按下selectDateBtn時判斷UIDataPicker狀態決定是否顯示出來
    if(_datePicker.hidden==YES){
        [_datePicker setHidden:NO];
    }else{
        [_datePicker setHidden:YES];
    }
}

- (IBAction)datePickerChange:(id)sender {
    //產生NSDateFormatter物件
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //指定顯示時間格式
    [dateFormatter setDateFormat:@"YYYY/MM/dd"];
    //dateFormatter呼叫stringFromDate，時間來自 _datePicker 目前選到的日期 date
    NSString *dateString = [dateFormatter stringFromDate:[_datePicker date]];
    //最後將dateString指派給Label供其顯示
    _showDate.text=dateString;
    
    //計算年齡
    //取得現在日期
    NSDate *now = [NSDate date];
    
    //宣告取得年
    NSCalendarUnit unitFlag = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    
    //NSDateComponents可用來計算時間差
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar] //用NSCalendar取得目前的Calendar
                                       components:unitFlag          //計算的部分取到年
                                       fromDate:[_datePicker date]      //Since什麼時候，必須由datePicker取值
                                       toDate:now              //到什麼時候
                                       options:0
                                       ];
    
    NSInteger nowAge = [ageComponents year];
    _age.text = [NSString stringWithFormat:@"%ld", (long)nowAge];
    
}
@end
